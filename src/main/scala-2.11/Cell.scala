/**
  * Created by jborkowski on 20/12/15.
  */
case class Cell(x: Int, y: Int) {
  override def hashCode = 44 * (44 + x) + y
  override def equals(other: Any) = other match {
    case that: Cell => this.x == that.x && this.y == that.y
    case _ => false
  }
}
