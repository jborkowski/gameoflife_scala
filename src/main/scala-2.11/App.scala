
import javafx.beans.property.SimpleBooleanProperty

import scalafx.animation.AnimationTimer
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application._
import scalafx.Includes._
import scalafx.event.ActionEvent
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.Button
import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.{AnchorPane, BorderPane}
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle

/**
  * Created by jborkowski on 20/12/15.
  * The UI isn't my a solution. I will change it.:)
  */
object App extends JFXApp{

  val running = new SimpleBooleanProperty()

  var lastTime = 0L

  // The timer object that will be used to update the board during the simulation
  val timer = AnimationTimer { now: Long =>
    // Push a new frame every 200 ms
    if (now - lastTime >= 200000000) {
      val nB = GameOfLife.nextGeneration(board)
      setBoard(nB.toSet)
      lastTime = now
    }
  }

  val startStopButton = new Button("Start") {
    // Conditional binding of ScalaFX properties
    text <== when(running) choose "Stop" otherwise "Start"
    // Handler for the button press event
    onAction = (p1: ActionEvent) => {
      running() = !running()
      if (running()) {
        timer.start()
      } else {
        timer.stop()
      }
    }
  }

  class BoardCell(val rx: Int, val ry: Int) extends Rectangle {
    // Whether or not the cell is alive
    val alive = new SimpleBooleanProperty()

    // A setter for the alive property
    def alive_=(v: Boolean) { alive() = v }

    // If the simulation is not running switch the status of the cell on click
    onMouseClicked = (p1: MouseEvent) => if (!running()) { alive() = !alive()}

    // Positioning and sizing properties
    x = rx * 10
    y = ry * 10
    width = 16
    height = 16

    // Nice rounded corners
    arcWidth = 4
    arcHeight = 4

    // Conditional property binding for the cell background colour
    fill <== when (hover && running.not()) choose Color.gray(0.6) otherwise (when (alive) choose Color.gray(0.9) otherwise Color.gray(0.1))
  }

  // Create a new cell instance with the given board coordinates and the given status
  def boardcell(rx: Int, ry: Int, a: Boolean = false) = new BoardCell(rx, ry) {
    alive() = a
  }

  val cells = for {
    rx <- 0 to 70
    ry <- 0 to 40
  } yield boardcell(rx, ry, false)

  // Creates a logical board from the current UI-board state
  def board: Set[Cell] = cells.filter(_.alive()).map(c => new Cell(c.rx, c.ry)).toSet

  // Updates the UI-board cells' status with the given logical board
  def setBoard(b: Set[Cell]): Unit = {
    cells.foreach(c => c.alive() = b.contains(new Cell(c.rx, c.ry)))
  }

  stage = new PrimaryStage {
    scene = new Scene {
      root = new BorderPane {
        fill = Color.gray(0)
        center = new AnchorPane {
          children = cells
        }
        bottom = new BorderPane {
          padding = Insets(5)
          center = startStopButton
        }
      }
    }
  }
}
