import scala.collection._

/**
  * Created by jborkowski on 20/12/15.
  */
object GameOfLife {

  def nextGeneration(board: Set[Cell]): Set[Cell] = {
    val rule1 = stayAliveOrKillCells(board)
    val rule2 = rescueCells(board)
    rule1 ++ rule2
  }

  def neighborAliveCell(cell: Cell, world: Set[Cell]) = world.contains(cell)

  def neighborDeadCell(cell: Cell, world: Set[Cell]) = !world.contains(cell)

  def stayAliveOrKillCells(world: Set[Cell]): Set[Cell] = world.filter(cell => {
          val numNeighbors = getNeighbor(cell, world, neighborAliveCell).size
          numNeighbors== 2 || numNeighbors == 3
  })

  def rescueCells(world: Set[Cell]): Set[Cell] = {
    val neighbors = world.flatMap(cell => getNeighbor(cell, world, neighborDeadCell))
    neighbors.filter(cell => {
      getNeighbor(cell, world, neighborAliveCell).size == 3
    })
  }

  val neighbourOffsets: Set[(Int, Int)] = (for {
    x <- -1 to 1
    y <- -1 to 1
  } yield (x, y)).toSet - ((0, 0))

  def getNeighbor(inputCell: Cell, world: Set[Cell], f:(Cell, Set[Cell]) => Boolean): Set[Cell] = {
    neighbourOffsets.map{case (x: Int, y: Int) => {
      new Cell(inputCell.x + x, inputCell.y + y)
    }}.filter(cell => f(cell, world))
  }

}
