import org.scalatest._

/**
  * Created by jborkowski on 20/12/15.
  */
class GameOfLifeTest extends FlatSpec with Matchers {

  "A method" should "be return new world, applying rules of game " in {
    // Rule 1: Any live cell with two or three live neighbours lives on to the next generation.
    // Rule 2: Any live cell with fewer than two live neighbours dies, as if caused by under-population.
    // Rule 3: Any live cell with more than three live neighbours dies, as if by overcrowding.
    // Rule 4: Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    val initList = Set( new Cell(4, 0),
                        new Cell(4, 2),
                        new Cell(5, 1),
                        new Cell(4, 1),
                        new Cell(5, 2))

    val world = GameOfLife.nextGeneration(initList)
    world.size should be (5)
  }
}
